#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <ctype.h>
#include <unistd.h>

#include "apriltag.h"
#include "image_u8.h"
#include "tag36h11.h"
#include "zarray.h"
#include "cv.h"
#include "highgui.h"

bool debug = true;
CvCapture* camera = NULL;
IplImage* iplImage = NULL;
image_u8_t *image_u8_create_from_image(IplImage* iplImage);
image_u8_t *image_u8_create_from_camera(CvCapture* camera);
void init_apriltag_detector(apriltag_detector_t **td, apriltag_family_t **tf);
void capture_image();

int main(int argc, char *argv[])
{
    apriltag_family_t *tf = NULL;
    apriltag_detector_t *td = NULL;
    init_apriltag_detector(&td , &tf);

    cvNamedWindow( "Win", CV_WINDOW_AUTOSIZE| CV_WINDOW_KEEPRATIO);
    if (!debug) {
        camera = cvCaptureFromCAM(0);
    }
    int key = cvWaitKey(5);
    while(key != 27) {
        capture_image();
        image_u8_t *im = image_u8_create_from_image(iplImage);

        if (im == NULL) {
            printf("couldn't find image\n");
            break;
        }


        zarray_t *detections = apriltag_detector_detect(td, im);
        for (int i = 0; i < zarray_size(detections); i++) {
            apriltag_detection_t *det;
            zarray_get(detections, i, &det);
            cvLine(iplImage, cvPoint(det->p[0][0], det->p[0][1]), cvPoint(det->p[1][0], det->p[1][1]), cvScalar(255,0,0,0), 4,8,0); 
            cvLine(iplImage, cvPoint(det->p[1][0], det->p[1][1]), cvPoint(det->p[2][0], det->p[2][1]), cvScalar(0,255,0,0), 4,8,0); 
            cvLine(iplImage, cvPoint(det->p[2][0], det->p[2][1]), cvPoint(det->p[3][0], det->p[3][1]), cvScalar(255,255,0,0), 4,8,0); 
            cvLine(iplImage, cvPoint(det->p[3][0], det->p[3][1]), cvPoint(det->p[0][0], det->p[0][1]), cvScalar(0,0,255,0), 4,8,0); 
            apriltag_detection_destroy(det);
        }
        cvShowImage("Win",iplImage);

        key = cvWaitKey(5);
        zarray_destroy(detections);
        image_u8_destroy(im);
    }
    apriltag_detector_destroy(td);
    cvReleaseImage(&iplImage);
    tag36h11_destroy(tf);
    if (!debug) {
        cvReleaseCapture(&camera);
    }    
    return 0;
}


image_u8_t *image_u8_create_from_image(IplImage* iplImage)
{
    image_u8_t *im = NULL;
    im = image_u8_create(iplImage->width, iplImage->height);
    printf("Width = %d \t Height = %d\t Channels = %d\n", iplImage->width, iplImage->height, iplImage->nChannels);
    
    // Gray conversion for RGB is gray = (r + g + g + b)/4
    for (int y = 0; y < im->height; y++) {
        for (int x = 0; x < im->width; x++) {
            uint8_t gray = 
                    (
                        ((uchar *)(iplImage->imageData + y * iplImage->widthStep))[x*iplImage->nChannels + 0] +
                        ((uchar *)(iplImage->imageData + y * iplImage->widthStep))[x*iplImage->nChannels + 1] +
                        ((uchar *)(iplImage->imageData + y * iplImage->widthStep))[x*iplImage->nChannels + 1] +
                        ((uchar *)(iplImage->imageData + y * iplImage->widthStep))[x*iplImage->nChannels + 2] 
                    )/4;
                    im->buf[y*im->stride + x] = gray;
        }
    }
    return im;
}

image_u8_t *image_u8_create_from_camera(CvCapture* camera)
{

    IplImage *iplImage = cvQueryFrame(camera);
    image_u8_t *im = NULL;
    im = image_u8_create(iplImage->width, iplImage->height);
    printf("Width = %d \t Height = %d\t Channels = %d\n", iplImage->width, iplImage->height, iplImage->nChannels);

    // Gray conversion for RGB is gray = (r + g + g + b)/4
    for (int y = 0; y < im->height; y++) {
        for (int x = 0; x < im->width; x++) {
            uint8_t gray =  
                    (
                        ((uchar *)(iplImage->imageData + y * iplImage->widthStep))[x*iplImage->nChannels + 0] +
                        ((uchar *)(iplImage->imageData + y * iplImage->widthStep))[x*iplImage->nChannels + 1] +
                        ((uchar *)(iplImage->imageData + y * iplImage->widthStep))[x*iplImage->nChannels + 1] +
                        ((uchar *)(iplImage->imageData + y * iplImage->widthStep))[x*iplImage->nChannels + 2] 
                    )/4;
                    im->buf[y*im->stride + x] = gray;
        }
    }
    return im;
}

void init_apriltag_detector(apriltag_detector_t **td, apriltag_family_t **tf) {
    *tf = tag36h11_create();
    *td = apriltag_detector_create();
    apriltag_detector_add_family(*td, *tf);
    (*td)->quad_decimate = 0.0;
    (*td)->quad_sigma = 0.0;
    (*td)->nthreads = 4;
    (*td)->debug = false;
    (*td)->refine_decode = false;
    (*td)->refine_pose = false;
}

void capture_image() {
    if (debug) {
        iplImage = cvLoadImage("tag.png", CV_LOAD_IMAGE_COLOR);
    } else {
        iplImage = cvQueryFrame(camera);
    }
}
